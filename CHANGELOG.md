#0.1.7 (30-05-2016)
- Added tags to profile using a placeholder for make custom colors and types.
- Added material avatar to components.
- Upload profile finished design.
- Upload reviews list design.
- Added profile nav to layouts.

Bugfixes:
- Added border-bottom to tabs.
- Changed `$buttons` array name by `$palette`.
- Footer links made bigger for better read.
- Remove columns styles for better display. Instead, it will not contain the content in full width (adds negative margin to all positions).

# 0.1.7 (29-05-2016)
- Added custom classes to tabs for position buttons inside.
- Added grid system for gallery using flexbox. Responsive design works fine in all devices.
- Added new variables to sass: breakpoints.
- Added new colors to arrays for generate dynamically more color buttons.
- Updated shot list v2: now have grid system working. Only grid has scroll and has responsive design.
- Tabs have two fixes: using `level` or using tabs `ul`. With level, tabs are positioned at left. With ul, in center.
- Global navbar is fixed to top using a custom class. Footer is positioned fixed in bottom.

Bugfixes:
- Fixed responsive in footer. Now works but should make it better.
- Fixed appear scrollbar in 404 template.
- Fixed problem with tabs: now can use buttons inside without hover problems.
- Remove 700 style from Roboto font because it will not be used. Instead use 500.

# 0.1.6 (2016-05-27)
- Upload logo in png for backup
- Designed logo as vector for export in SVG
- Added SVG logo to navbar
- Added logo versions in styleguide .sketch file

Bugfixes:
- Final version of search bar for best accessibility
- Hover effects in nav items
- Footer links and hover effect

# 0.1.5 (2016-05-27)  
Bugfixes:
- Fix tabs font-size for best read

# 0.1.4 (2016-05-27)  
- Final version of global navbar, tabs and mini-footer (Task **#23**)

# 0.1.3 (2016-05-26)  
- Start shot list v1 and v2 (Task **#23**)

# 0.1.2 (2016-05-25)  
- Upload v2 of 404 page and 505 page in HTML (Task **#11** and **#49**)

# 0.1.1 (2016-05-20)  
- Upload .sketch file for backup all sketch designs

# 0.1.0 (2016-05-9)  
- Add bulma.io for use as base

# 0.0.2 (2016-05-5)  
- Upload v1 of 404 page (Task **#11**)

# 0.0.1 (2016-05-5)  
- First upload
