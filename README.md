# Design of Critique project
This is the repo for the Design part of Critique project.

## What you will find
- Templates with its code, style and scripts
- Sketch source files with all the designs

## What you will not find
You will not find any `js` file with logic. For that, go to the `critique-project` repo. Develop there all your React components, database and Meteor app!

## What to do with this
Convert all of this code into React components in the `critique-project` repo, for make it reusable and `mini-tricky` pieces of code.

## Styleguides and conventions used
### CSS & SASS
- [CSS Guidelines](http://cssguidelin.es/)
- [Sass Guidelines](http://sass-guidelin.es/)
- [Airbnb CSS/Sass styleguide](https://github.com/airbnb/css)
- [7-1 pattern defined in Sass Guidelin](http://sass-guidelin.es/#architecture)

## Cheatsheet
### Style Guide
*What is a Style Guide?*
Is a document which details how to organize, structure, format and write Sass.

> Contains the DO's and DONT's to use SASS in the project based on the designer's taste.

Are in `docs/` folder. Name of the file: `styleguide.md`.

### Documentation
*What is a Documentation?*
Is a document that shows how to use a library or framework.

> Contains all the mixins, functions and variables used in SASS and documented thoroughly.

Are in `docs/sassdoc/` folder. Name of the file: `index.html`.

There you will see mixins and functions which can contain a lot of annotiations, but the basics and must-have are:
- `Description`
- `Parameters` name, description, type and default value
- `Author`
- `Example` use case of it
- `Output` output of the mixin used
- `Group` to which belongs
- `Parameter` parameters used

With variables, you will find:
- `Description`
- `Type` if it's a color
- If it's a color, you will see the color behind the variable.

### Pattern Library
*What is a Pattern Library?*
Is a collection of components, modules and patterns with the code and visual style behind them. Also, named **living document**.

> Contains all the pieces that create your front-end UI.

Are in `docs/` folder. Name of the file: `styleguide.html`.

There you will see:
- An introduction to that styleguide with name and description
- Each component used in the application grouped by type. `Eg. btn-group and btn are inside Buttons group`
- In component you have: a description of that component, the class used, an example of use and the code

## Used tools
`Full package use`
- [Meteor as Framework](http://www.meteor.com)
- [Mongo as Database](https://www.mongodb.org/)
- [React as View Layer](https://facebook.github.io/react/)

`Some parts of package used`
- Add here your tools like `[Name of the tool](Url)`

`Documentation`
- [SassDoc for Documentation](http://sassdoc.com/)
- [React styleguidist for React Components documentation](https://github.com/sapegin/react-styleguidist)

`Software online`
- [GitLab as Git repository](https://gitlab.com/)
- [Draw as UML modeling tool](http://draw.io)
- [Trello as Kanban board with Chrome addons:](https://trello.com/)
	- [card color titles for trello](https://chrome.google.com/webstore/detail/card-color-titles-for-tre/hpmobkglehhleflhaefmfajhbdnjmgim)
    - [punchtime for trello](https://chrome.google.com/webstore/detail/punchtime-for-trello/ojccjdibbkolgodijhgfgmcdgchlojao)
    - [trello card numbers](https://chrome.google.com/webstore/detail/trello-card-numbers/kadpkdielickimifpinkknemjdipghaf)

`Software offline`
- [Sketch as prototype tool](https://www.sketchapp.com/)
- [Atom as text editor](https://atom.io/)
- [Git as control version](https://git-scm.com/)

## References
=======
